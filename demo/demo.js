const app = angular.module('demo', ['topDevs']);

class MainController {
  constructor($rootScope) {
    this.controls = [{},{},{}];
    this.settings = {
      mode: "playback",
      baseUrl: "https://cdn.snap.menu/version"
    };
    this.campaign = {
      "modified": "2017-02-28T23:21:58.279Z",
      "location": "8af78c7b-b391-44ff-a538-a64101188338",
      "canvas": {
        "orientation": "landscape",
        "width": 1,
        "height": 1
      },
      "title": "GA-Brunswick",
      "slides": [
        {
          "transition": "fade",
          "delay": 5,
          "elements": [
            {
              "name": "72ndAnnBrunswick-01.jpg",
              "media": {
                "height": 1080,
                "width": 1920,
                "token": "3b5b525b-ac6f-415a-a958-38fa9d318420",
                "mime": "image/jpeg"
              },
              "layout": {
                "bottom": 0,
                "right": 0,
                "top": 0,
                "left": 0
              },
              "type": "media",
              "medias": [

              ],
              "position": 0,
              "token": "58b6052bec5ddea8c1729b06"
            }
          ],
          "position": 0,
          "token": "58974014ec5dde2792d3fa98"
        },
        {
          "transition": "fade",
          "delay": 5,
          "elements": [
            {
              "type": "media",
              "layout": {
                "left": 0,
                "top": 0,
                "right": 0,
                "bottom": 0
              },
              "media": {
                "mime": "image/jpeg",
                "token": "52167b08-6cd6-4116-8cd6-2a45b208d36c",
                "width": 1920,
                "height": 1080
              },
              "name": "72ndAnnBrunswick-02.jpg",
              "medias": [

              ],
              "position": 0,
              "token": "58b6053eec5ddea8c1729b07"
            }
          ],
          "position": 0,
          "token": "58974017ec5dde2792d3fa99"
        },
        {
          "transition": "fade",
          "delay": 5,
          "elements": [
            {
              "name": "72ndAnnBrunswick-03.jpg",
              "media": {
                "height": 1080,
                "width": 1920,
                "token": "90c1bbd2-4a0f-4edd-863d-d644068298ac",
                "mime": "image/jpeg"
              },
              "layout": {
                "bottom": 0,
                "right": 0,
                "top": 0,
                "left": 0
              },
              "type": "media",
              "medias": [

              ],
              "position": 0,
              "token": "58b60555ec5ddea8c1729b08"
            }
          ],
          "position": 0,
          "token": "58974019ec5dde2792d3fa9a"
        },
        {
          "transition": "fade",
          "delay": 5,
          "elements": [
            {
              "type": "media",
              "layout": {
                "left": 0,
                "top": 0,
                "right": 0,
                "bottom": 0
              },
              "media": {
                "mime": "image/jpeg",
                "token": "708fab36-c3de-4044-9059-84011295e118",
                "width": 1920,
                "height": 1080
              },
              "name": "72ndAnnBrunswick-04.jpg",
              "medias": [

              ],
              "position": 0,
              "token": "58b6056fec5ddea8c1729b09"
            }
          ],
          "position": 0,
          "token": "5897401aec5dde2792d3fa9b"
        },
        {
          "transition": "fade",
          "delay": 5,
          "elements": [
            {
              "name": "72ndAnnBrunswick-05.jpg",
              "media": {
                "height": 1080,
                "width": 1920,
                "token": "86644f26-166a-4130-aa87-1268433f6e7e",
                "mime": "image/jpeg"
              },
              "layout": {
                "bottom": 0,
                "right": 0,
                "top": 0,
                "left": 0
              },
              "type": "media",
              "medias": [

              ],
              "position": 0,
              "token": "58b60586ec5ddea8c1729b0a"
            }
          ],
          "position": 0,
          "token": "5897401cec5dde2792d3fa9c"
        },
        {
          "transition": "fade",
          "delay": 5,
          "elements": [
            {
              "type": "media",
              "layout": {
                "left": 0,
                "top": 0,
                "right": 0,
                "bottom": 0
              },
              "media": {
                "mime": "image/jpeg",
                "token": "93082988-4363-4bad-b429-42fd2c9e9020",
                "width": 1920,
                "height": 1080
              },
              "name": "72ndAnnBrunswick-06.jpg",
              "medias": [

              ],
              "position": 0,
              "token": "58b6059bec5ddea8c1729b0b"
            }
          ],
          "position": 0,
          "token": "5897401eec5dde2792d3fa9d"
        },
        {
          "transition": "fade",
          "delay": 5,
          "elements": [
            {
              "name": "72ndAnnBrunswick-07.jpg",
              "media": {
                "height": 1080,
                "width": 1920,
                "token": "b715728c-98c1-4eff-912a-b6f61ba860b8",
                "mime": "image/jpeg"
              },
              "layout": {
                "bottom": 0,
                "right": 0,
                "top": 0,
                "left": 0
              },
              "type": "media",
              "medias": [

              ],
              "position": 0,
              "token": "58b605b6ec5ddea8c1729b0c"
            }
          ],
          "position": 0,
          "token": "5897401fec5dde2792d3fa9e"
        },
        {
          "transition": "fade",
          "delay": 5,
          "elements": [
            {
              "type": "media",
              "layout": {
                "left": 0,
                "top": 0,
                "right": 0,
                "bottom": 0
              },
              "media": {
                "mime": "image/jpeg",
                "token": "ed2eb207-1cec-4ec7-a841-a1a674687f58",
                "width": 1920,
                "height": 1080
              },
              "name": "72ndAnnBrunswick-08.jpg",
              "medias": [

              ],
              "position": 0,
              "token": "58b605c9ec5ddea8c1729b0d"
            }
          ],
          "position": 0,
          "token": "58974020ec5dde2792d3fa9f"
        },
        {
          "delay": 5,
          "transition": "fade",
          "elements": [
            {
              "type": "media",
              "layout": {
                "left": 0,
                "top": 0,
                "right": 0,
                "bottom": 0
              },
              "media": {
                "mime": "image/jpeg",
                "token": "03fc707c-12b3-46e9-8764-3d1a05f0ecaf",
                "width": 1920,
                "height": 1080
              },
              "name": "72ndAnnBrunswick-09.jpg",
              "medias": [

              ],
              "position": 0,
              "token": "58b605e8ec5ddea8c1729b11"
            }
          ],
          "position": 0,
          "token": "58b605d6ec5ddea8c1729b0e"
        },
        {
          "delay": 5,
          "transition": "fade",
          "elements": [
            {
              "name": "72ndAnnBrunswick-10.jpg",
              "media": {
                "height": 1080,
                "width": 1920,
                "token": "8a588086-e79e-402d-a398-b64dc2b2aff6",
                "mime": "image/jpeg"
              },
              "layout": {
                "bottom": 0,
                "right": 0,
                "top": 0,
                "left": 0
              },
              "type": "media",
              "medias": [

              ],
              "position": 0,
              "token": "58b60604ec5ddea8c1729b12"
            }
          ],
          "position": 0,
          "token": "58b605d8ec5ddea8c1729b0f"
        },
        {
          "delay": 328,
          "transition": "fade",
          "elements": [
            {
              "type": "media",
              "layout": {
                "left": 0,
                "top": 0,
                "right": 0,
                "bottom": 0
              },
              "media": {
                "mime": "video/webm",
                "token": "3d039037-0db4-4265-a6b6-87a23f96d3f8",
                "width": 1920,
                "height": 1080
              },
              "name": "AHSJAX 72 Anniversary DVD.webmhd.webm",
              "medias": [

              ],
              "position": 0,
              "token": "58b60612ec5ddea8c1729b13"
            }
          ],
          "position": 0,
          "token": "58b605daec5ddea8c1729b10"
        }
      ],
      "player_devices": [
        "8113e74b-c0d1-4ed5-b896-3befc3797674",
        "608e057b-a1cd-49d2-93c6-e1765c5928dc",
        "c07ff7fe-2213-4796-a66c-8f2e55ea2fe1",
        "66bc35fe-745c-45b3-943d-cd18cc5e2664"
      ],
      "position": 0,
      "token": "5818c0399a0a632400805190"
    };

    this.$rootScope = $rootScope;
    this.$rootScope.$on("loaded", () => {
      console.log('MainController loaded');
    });

    this.$rootScope.$on("clicked", (event, data) => {
      console.log('MainController', event, data);
    })
  }
}

MainController.$inject = ['$rootScope'];
app.controller('MainController', MainController);
