'use strict';

class TopDevsController {
  constructor($http, $q, $rootScope, $element) {
    this.$http = $http;
    this.$q = $q;
    this.$rootScope = $rootScope;
    this.$element = $element;

    this.isVisible = false;

    this.formats = {
      "image/jpeg": "jpg",
      "image/png": "png",
      "video/webm": "mp4"
    };
  }

  $onChanges(obj) {
    if (obj.slide && !obj.slide.isFirstChange()) {
      //TODO если необходимо изменить порядок едементов, необходимо переделать реализацию
      obj.slide.currentValue.elements.forEach((newElement) => {
        if (!obj.slide.previousValue.elements.includes(newElement)) {
          this.loadElement(newElement);
        }
      });
    }
  }

  $onInit() {
    if (this.controls) {
      this.controls.play = this.play;
      this.controls.pause = this.pause;
    } else {
      console.log("controls object is absent");
    }

    let promises = this.loadElements();

    this.$q.all(promises)
      .then(() => {
        this.isVisible = true;
        this.$rootScope.$emit("loaded");
      })
      .catch((err) => console.log(err));
  }

  $postLink() {
    this.$element.on("click", () => {
      this.$rootScope.$emit("clicked", this.slide);
    });
  }

  $onDestroy() {
    this.$element.off("click");
  }

  loadImage(media) {
    let img = new Image();
    let promise = this.$q((resolve) => {
      img.onload = () => resolve()
    })
      .then(() => media.src = img.src);

    img.src = this.getUrl(media);
    return promise;
  }

  loadVideo(media) {
    let video = document.createElement("video");
    video.type = "video/mp4";

    let promise = this.$q((resolve) => {
      video.oncanplay = () => resolve();
    })
      .then(() => media.src = video.src);
    video.src = this.getUrl(media);
    video.load();
    return promise;
  }

  loadElement(element) {
    let media = element.media;
    let promise;
    if (this.isImage(media.mime)) {
      promise = this.loadImage(media);
    } else if (this.isVideo(media.mime)) {
      promise = this.loadVideo(media);
    }
    return promise;
  }

  loadElements() {
    return this.slide.elements.map((elm) => this.loadElement(elm));
  }

  getUrl(media) {
     return `${this.settings.baseUrl}/${media.token}_${media.width}_${media.height}.${this.getFormat(media.mime)}`;
  }

  getFormat(mimeType) {
    if (this.settings.mode === "view") {
      return "jpg";
    }

    return this.formats[mimeType];
  }

  isImage(mimeType) {
    return this.getFormat(mimeType) !== "mp4";
  }
  isVideo(mimeType) {
    return this.getFormat(mimeType) === "mp4";
  }

  play() {
    console.log('play');
  }

  pause() {
    console.log("pause");
  }
}

TopDevsController.$inject = ['$http', '$q', '$rootScope', '$element'];

angular.module('topDevs', []).component('topDevsComponent', {
  controller: TopDevsController,
  templateUrl: 'component.html',
  bindings: {
    slide: '<',
    settings: '<',
    controls: '<'
  }
});