const fs = require('fs');
const connect = require('gulp-connect');
const gulp = require('gulp');
const karma = require('karma').server;
const concat = require('gulp-concat');
const header = require('gulp-header');
const rename = require('gulp-rename');
const es = require('event-stream');
const del = require('del');
const uglify = require('gulp-uglify');
const minifyHtml = require('gulp-minify-html');
const templateCache = require('gulp-angular-templatecache');
const plumber = require('gulp-plumber');
const open = require('gulp-open');
const order = require("gulp-order");
const babel = require('gulp-babel');


const config = {
  pkg : JSON.parse(fs.readFileSync('./package.json')),
  banner:
      '/*!\n' +
      ' * <%= pkg.name %>\n' +
      ' * <%= pkg.homepage %>\n' +
      ' * Version: <%= pkg.version %> - <%= timestamp %>\n' +
      ' * License: <%= pkg.license %>\n' +
      ' */\n\n\n'
};

gulp.task('connect', function() {
  connect.server({
    root: [__dirname],
    livereload: true
  });
});

gulp.task('html', function () {
  gulp.src(['./demo/*.html', '.src/*.html'])
    .pipe(connect.reload());
});

gulp.task('watch', function () {
  gulp.watch(['./demo/**/*.html'], ['html']);
  gulp.watch(['./src/**/*.js','./demo/**/*.js', './**/*.html'], ['scripts']);
});

gulp.task('clean', function(cb) {
  del(['dist/**/*'], cb);
});

gulp.task('scripts', ['clean'], function() {

  function buildTemplates() {
    return gulp.src('src/**/*.html')
      .pipe(minifyHtml({
             empty: true,
             spare: true,
             quotes: true
            }))
      .pipe(templateCache({module: 'topDevs'}));
  };

  function buildDistJS(){
    return gulp.src('src/component.js')
      .pipe(babel({
        presets: ['es2015']
      }))
      .pipe(plumber({
        errorHandler: handleError
      }))
  }

  es.merge(buildDistJS(), buildTemplates())
    .pipe(plumber({
      errorHandler: handleError
    }))
    .pipe(order([
      'component.js',
      'template.js'
    ]))
    .pipe(concat('component.js'))
    .pipe(header(config.banner, {
      timestamp: (new Date()).toISOString(), pkg: config.pkg
    }))
    .pipe(gulp.dest('dist'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify({preserveComments: 'some'}))
    .pipe(gulp.dest('./dist'))
    .pipe(connect.reload());
});

gulp.task('open', function(){
  gulp.src('./demo/demo.html')
  .pipe(open('', {url: 'http://localhost:8080/demo/demo.html'}));
});


function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}

gulp.task('build', ['scripts']);
gulp.task('serve', ['build', 'connect', 'watch', 'open']);
gulp.task('default', ['build']);
